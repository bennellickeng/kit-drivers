from prologix import Prologix

class MG3660A(object):
    MIN_F = 300000
    MAX_F = 2750000000

    def __init__(self, ip, device_address=16, read_f_once=False):
        self.device = Prologix(ip, device_address, termination_char=Prologix.TERM_NONE, opc_before_command=True,
                               read_timeout=500)
        self.name = self.device.get_name()
        self._read_f_once = read_f_once
        self._frequencies = None
        print(self.name)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.device.opc_wait("WAIT", timeout=2)
        self.local()

    def local(self):
        self.device.local()

    def preset(self):
        self.device.write("PRES")
        self.device.opc_wait("WAIT", timeout=2)


    def frequency(self, f):
        assert(f >= self.MIN_F and f <= self.MAX_F)
        self.device.write("FREQ {:d}".format(int(f)))


if __name__ == '__main__':
    sg = MG3660A("192.168.1.193", device_address=1)
    sg.frequency(10000000)

