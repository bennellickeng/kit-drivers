import time
import visa
import numpy as np


class Multi_Meter_dmm6500(object):
    """ A driver for the Keithley DMM65500 multimeter
    """
    class Mode:
        CURRENT    = 0
        RESISTANCE = 1
        VOLTAGE    = 2
        DIGITIZE_VOLTAGE = 3

        @staticmethod
        def string(mode):
            if mode == Multi_Meter_dmm6500.Mode.CURRENT:
                return "CURR"

            if mode == Multi_Meter_dmm6500.Mode.RESISTANCE:
                return "RES"

            if mode == Multi_Meter_dmm6500.Mode.VOLTAGE:
                return "VOLT"

            if mode == Multi_Meter_dmm6500.Mode.DIGITIZE_VOLTAGE:
                return "DIG:VOLT"

            return "UNKNOWN_MODE"

    def __init__(self, serial_number, mode):
        """ Connect to usbtmc and setup our internal mode string """
        rm  = visa.ResourceManager("@py")
        #self.device = Usbtmc.from_serial_number(serial_number)
        self.device = rm.open_resource("USB0::1510::25856::{}::0::INSTR".format(serial_number))
        self.device.clear()
        self.device.write("*RST")
        self.device.write("ABORT")
        self.device.clear()
        self.name = self.device.query("*IDN?")
        self.set_mode(mode)

        print(self.name)
        print("Mode '{0}'".format(self.mode_str))

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()

    def close(self):
        print("Closing {}".format(self.device))
        self.device.close()

    def set_mode(self, mode):
        """ Set our mode string and setup the device to
            the correct mode
        """
        self.mode_str = Multi_Meter_dmm6500.Mode.string(mode)
        if "DIG" in self.mode_str:
            self.device.write("SENS:DIG:FUNC \"{0}\"".format(self.mode_str[4:]))
        else:
            self.device.write("SENS:FUNC \"{0}\"".format(self.mode_str))

    def set_buffer_size(self, n_points):
        self.device.write("TRACE:POINTS {}".format(int(n_points)))

    def clear_buffer(self):
        self.device.write("TRAC:CLE")
        self.device.write("*WAI")

    def set_power_line_sync(self, is_on):
        """ Set the power line sync for our mode """
        self.device.write("{0}:LINE:SYNC {1}".format(
            self.mode_str, "ON" if is_on else "OFF"            
        ))

    def set_nplc(self, rate):
        """ set power line rate for our mode """
        self.device.write("{0}:NPLC {1}".format(
            self.mode_str, rate
        ))

    def set_digitize_rate(self, rate):
        """ Set the digitizing sample rate in Hz """
        self.device.write("SENS:{}:SRAT {}".format(
            self.mode_str, rate
        ))

    def set_digitize_range(self, rate):
        """ Set the digitizing range """
        self.device.write("SENS:{}:RANGE {}".format(
            self.mode_str, rate
        ))

    def set_digitize_count(self, count):
        """ Set the number of samples to take when digitizing """
        self.device.write("SENS:DIG:COUN {}".format(count))

    def wait_for_trigger_then_measure(self, measurement_count=1, sleep_time=0.01):
        """ Wait for an input trigger then do a single
            measurement into our default buffer
        
            note: sleeps for a small time to ensure the trigger 
            is active
        """
        # we start by defining our trigger model
        # clear any old model
        self.device.write("TRIG:LOAD \"EMPTY\"")

        # clear any unflushed EXT events
        self.device.write("TRIG:EXT:IN:CLEAR")

        # listen for rising edge trigger
        self.device.write("TRIG:EXT:IN:EDGE RIS")

        # wait for the EXT event
        self.device.write("TRIG:BLOC:WAIT 1, EXT")

        # perform requested number of measurements
        self.device.write("TRIG:BLOC:MDIG 2, \"defbuffer1\", {}".format(measurement_count))

        # start the above trigger model
        time.sleep(0.01)
        self.device.write("INIT")
        self.device.write("*WAI")

        time.sleep(sleep_time)

    def send_trigger_then_measure(self, measurement_count=1):
        """ Notify an output trigger then do a single 
            measurement
        """
        self.device.write("TRIG:LOAD \"EMPTY\"")

        # set our external out stimulus to be 'NOTIFY1'
        self.device.write("TRIG:EXT:OUT:STIM NOT1")

        # send 'NOTIFY1'
        self.device.write("TRIG:BLOC:NOT 1, 1")

        # perform requested number of measurements
        self.device.write("TRIG:BLOC:MDIG 2, \"defbuffer1\", {}".format(measurement_count))

        time.sleep(0.01)
        self.device.write("INIT")
        self.device.write("*WAI")

    def read_single_value(self, clear_after_read=True):
        """ read a single value from the default 
            buffer and return it as a float
        """
        res = self.device.query_ascii_values("TRAC:DATA? 1, 1")
        if clear_after_read:
            self.clear_buffer()
        return res

    def read_all_values(self, clear_after_read=True):
        """ read all values from the default
            buffer and return them as a list of floats
        """
        start_idx = int(self.device.query("TRAC:ACT:STAR?"))

        end_idx = int(self.device.query("TRAC:ACT:END?"))

        res = self.device.query_ascii_values("TRAC:DATA? {}, {}".format(start_idx, end_idx), container=np.array)
        if clear_after_read:
            self.clear_buffer()
        return res
