from __future__ import print_function
import sys
import time
from bennellickeng.kitdrivers.usbtmc.dc_load_array_3720a import DC_Load_Array_3720a as DCL


def measure():
    print(load.get_voltage())
    print(load.get_current())
    print(load.get_resistance())
    print(load.get_power())
    print()




load = DCL("/dev/usbtmc0")

load.reset()

time.sleep(1)
load.set_mode(load.Mode.CPV)
load.set_power(100)

load.input_enable()
time.sleep(1)
load.input_disable()
time.sleep(1)

load.set_mode(load.Mode.CRH)
load.set_resistance(2000)
measure()

time.sleep(1)

load.set_mode(load.Mode.CV)
load.set_voltage(20)
measure()

time.sleep(1)
load.set_mode(load.Mode.CCH)
load.set_current(20.4)
measure()




